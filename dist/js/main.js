"use strict";

function _typeof(e) {
    return (_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e
    } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
    })(e)
}

function ownKeys(t, e) {
    var r, o = Object.keys(t);
    return Object.getOwnPropertySymbols && (r = Object.getOwnPropertySymbols(t), e && (r = r.filter(function (e) {
        return Object.getOwnPropertyDescriptor(t, e).enumerable
    })), o.push.apply(o, r)), o
}

function _objectSpread(t) {
    for (var e = 1; e < arguments.length; e++) {
        var r = null != arguments[e] ? arguments[e] : {};
        e % 2 ? ownKeys(Object(r), !0).forEach(function (e) {
            _defineProperty(t, e, r[e])
        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : ownKeys(Object(r)).forEach(function (e) {
            Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(r, e))
        })
    }
    return t
}

function _defineProperty(e, t, r) {
    return (t = _toPropertyKey(t)) in e ? Object.defineProperty(e, t, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : e[t] = r, e
}

function _toPropertyKey(e) {
    e = _toPrimitive(e, "string");
    return "symbol" === _typeof(e) ? e : String(e)
}

function _toPrimitive(e, t) {
    if ("object" !== _typeof(e) || null === e) return e;
    var r = e[Symbol.toPrimitive];
    if (void 0 === r) return ("string" === t ? String : Number)(e);
    r = r.call(e, t || "default");
    if ("object" !== _typeof(r)) return r;
    throw new TypeError("@@toPrimitive must return a primitive value.")
}

var a = {a: "Hello"}, b = {b: "123456789"}, c = _objectSpread(_objectSpread({}, a), b);
console.log(c);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOlsiYSIsImIiLCJjIiwiX29iamVjdFNwcmVhZCIsImNvbnNvbGUiLCJsb2ciXSwibWFwcGluZ3MiOiI0MkNBQUEsSUFBTUEsRUFBSSxDQUFDQSxFQUFHLE9BQU8sRUFDZkMsRUFBSSxDQUFDQSxFQUFHLFdBQVcsRUFFbkJDLEVBQUNDLGNBQUFBLGNBQUEsR0FBT0gsQ0FBQyxFQUFLQyxDQUFDLEVBRXJCRyxRQUFRQyxJQUFJSCxDQUFDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBhID0ge2E6IFwiSGVsbG9cIn07XG5jb25zdCBiID0ge2I6IFwiMTIzNDU2Nzg5XCJ9O1xuZGVidWdnZXI7XG5jb25zdCBjID0gey4uLmEsIC4uLmJ9O1xuXG5jb25zb2xlLmxvZyhjKTsgLy8ge2E6IFwiSGVsbG9cIiwgYjogXCJXb3JsZFwifSJdfQ==
